from django.shortcuts import render
from .models import Detail

def home(request):
    details = Detail.objects.all()

    context = {
        'details' : details
    }

    return render(request, 'story7/index.html', context)