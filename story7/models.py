from django.db import models

class Detail(models.Model):
    title = models.CharField(max_length=30)
    detail = models.TextField(max_length=300, default="Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium sit quisquam soluta, recusandae dolorem ea aspernatur deserunt exercitationem doloremque. Inventore facere sequi quos ex itaque, asperiores harum culpa commodi! Nemo!")
    color = models.CharField(max_length = 20, default='beige')

    def __str__(self):
        return self.title

def populate():
    titles = ['Current Activities', 'Achievements', 'Hobbies and Interests', 'Work Experiences']
    colors = ['salmon', 'hotpink', 'coral', 'peachpuff', 'plum', 'palegreen', 'aquamarine', 'bisque', 'lightgray', 'beige']
    for i in range(len(titles)):
        Detail.objects.create(title=titles[i], color=colors[i%10])