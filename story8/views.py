from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.utils.datastructures import MultiValueDictKeyError
import requests
import json


def home(request):

    context = {}
    return render(request, 'story8/index.html', context)


def cari_buku(request):
    try:
        url = 'https://www.googleapis.com/books/v1/volumes/?q=' + \
            request.GET['q'] + '&maxResults=25'
        ret = requests.get(url)
        data = json.loads(ret.content)

        return JsonResponse(data, safe=False)
    except MultiValueDictKeyError:
        return redirect('story8:home')
