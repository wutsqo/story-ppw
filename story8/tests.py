from django.test import TestCase, Client
from django.urls import reverse
from django.apps import apps
from .apps import Story8Config
import requests, json


class TestUrls(TestCase):
    def setUp(self):
        self.home = reverse('story8:home')
        self.cari = reverse('story8:cari')

    def test_url_home(self):
        self.assertEqual(self.home, '/buku/')

    def test_url_cari(self):
        self.assertEqual(self.cari, '/buku/cari')


class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')


class TestViews(TestCase):
    def setUp(self):
        self.home = reverse('story8:home')
        self.cari = reverse('story8:cari')

    def test_GET_home(self):
        response = self.client.get(self.home)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story8/index.html')

    def test_GET_cari_with_parameter(self):
        response = self.client.get(self.cari + "?q=9786020384160")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['items'][0]['volumeInfo']['title'], 'Daun yang Jatuh Tak Pernah Membenci Angin (Cover Baru)')

    def test_GET_cari_no_parameter(self):
        response = self.client.get(self.cari, follow=True)
        self.assertRedirects(response, self.home, status_code=302,
                             target_status_code=200, fetch_redirect_response=200)

    def test_return_same_json(self):
        data_langsung = json.loads(requests.get('https://www.googleapis.com/books/v1/volumes/?q=hujan&maxResults=25').content)
        data_perantara = json.loads(self.client.get(self.cari+'?q=hujan').content)
        self.assertEqual(data_langsung, data_perantara)