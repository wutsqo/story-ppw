from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.URLField(default="https://robohash.org/default?set=set4")
    full_name = models.CharField(max_length=50, null=True, blank=True)
    rep_points = models.IntegerField(default=0)
    instagram = models.CharField(max_length=50, null=True, blank=True)
    twitter = models.CharField(max_length=50, null=True, blank=True)
    line = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return f'{self.user.username}'
