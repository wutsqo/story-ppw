from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.home, name='home'),
    path('signup/', views.signup, name='signup'),
    path('login/', LoginView.as_view(template_name='story9/login.html'), name='login'),
    path('logout/', LogoutView.as_view(template_name='story9/logout.html'), name='logout'),
    path('profile/', views.profile, name='profile'),
]
