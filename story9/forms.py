from django import forms
from crispy_forms.helper import FormHelper

from .models import Profile


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = '__all__'
        exclude = ['user', 'rep_points']
        widgets = {
            'full_name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'id': 'full-name',
                    'placeholder': 'Enter your full name',
                }
            ),
            'instagram': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'id': 'instagram',
                    'placeholder': 'Enter your Instagram account (e.g. @myigaccount)',
                }
            ),
            'twitter': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'id': 'twitter',
                    'placeholder': 'Enter your Twitter account (E.g. @mytwitteraccount)',
                }
            ),
            'line': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'id': 'line',
                    'placeholder': 'Enter your Line id (e.g terlalutampan)',
                }
            ),
            'avatar': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'id': 'image',
                    'placeholder': 'Enter an image URL',
                }
            ),
        }

    def __init__(self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.fields['full_name'].label = "Full Name"
        self.fields['instagram'].label = "Instagram"
        self.fields['twitter'].label = "Twitter"
        self.fields['line'].label = "Line"
