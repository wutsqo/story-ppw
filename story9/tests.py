from django.test import TestCase
from django.urls import resolve, reverse
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.models import User
from django.apps import apps
from .views import home, signup, profile
from .apps import Story9Config
from .models import Profile


class TestApp(TestCase):
    def test_app(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')


class TestView(TestCase):
    def setUp(self):
        self.home = reverse('story9:home')
        self.login = reverse('story9:login')
        self.logout = reverse('story9:logout')
        self.signup = reverse('story9:signup')
        self.profile = reverse('story9:profile')
        self.user = User.objects.create_user(
            'dosan', 'dosan@mail.com', 'samsantec')

    def test_GET_home(self):
        response = self.client.get(self.home)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/home.html')

    def test_GET_login(self):
        response = self.client.get(self.login)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_GET_signup(self):
        response = self.client.get(self.signup)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/signup.html')

    def test_POST_signup(self):
        response = self.client.post(self.signup, {
            'username': 'mouse',
            'password1': 'logig103',
            'password2': 'logig103'
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(User.objects.all().count(), 2)

    def test_GET_logout(self):
        response = self.client.get(self.logout)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/logout.html')

    def test_GET_profile_not_authenticated(self):
        response = self.client.get(self.profile, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_GET_profile_authenticated(self):
        self.client.login(username='dosan', password='samsantec')
        response = self.client.get(self.profile)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/profile.html')

    def test_POST_profile(self):
        self.client.login(username='dosan', password='samsantec')
        response = self.client.post(self.profile, data={
            'avatar' : 'https://assets.stickpng.com/images/5845cd230b2a3b54fdbaecf7.png',
            'instagram': 'dosan',
            'full_name': 'Nam Do San'
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/profile.html')
        self.assertContains(response, 'Nam Do San')


class TestModels(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            'dosan', 'dosan@mail.com', 'samsantec')

    def test_create_user(self):
        self.assertEqual(User.objects.all().count(), 1)

    def test_profile(self):
        self.assertEqual(Profile.objects.all().count(), 1)

    def test_str_profile(self):
        self.assertEqual(str(self.user.profile), 'dosan')
