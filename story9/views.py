from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import EditProfileForm
from django.contrib.auth.models import User


def home(request):
    context = {}
    return render(request, 'story9/home.html', context)


def signup(request):
    context = {}
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.add_message(
                request, messages.SUCCESS, f'Succesfully registered {username}. You can now log in.')
            return redirect('story9:login')
    else:
        form = UserCreationForm()
    context = {
        'form': form,
    }
    return render(request, 'story9/signup.html', context)


@login_required
def profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user.profile)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS,
                                 f'Successfully updated profile.')
            return redirect('story9:profile')
    else:
        form = EditProfileForm(instance=request.user.profile)
    context = {
        'form': form,
    }
    return render(request, 'story9/profile.html', context)
