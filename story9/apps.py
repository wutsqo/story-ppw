from django.apps import AppConfig


class Story9Config(AppConfig):
    name = 'story9'

    def ready(self):
        import story9.signals
